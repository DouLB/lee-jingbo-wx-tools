// app/pages/component-page/c-navi/index.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        naviType: "normal", // normal | slot | search
        listHeight: 0,
        statusBarHeight: 0
    },
    naviHeightChange(e) {
        this.setData({
            listHeight: wx.getSystemInfoSync().windowHeight - e.detail.height,
            statusBarHeight: e.detail.statusBarHeight
        })
    },
    changeNavi() {
        let naviTypeList = ["normal", "slot", "search"];
        let targetIndex = 0;
        naviTypeList.forEach((item, index)=>{
            if (item == this.data.naviType) {
                targetIndex = index+1 == naviTypeList.length ? 0 : index+1
            }
        })
        this.setData({
            naviType: naviTypeList[targetIndex]
        })
    }
})