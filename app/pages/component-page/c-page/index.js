const { KEY_NOTIFICATION_PAGE_NOTIFY_SHOW } = require("../../../../dist/keys/notification-key")
const { postNotification } = require("../../../../dist/utils/notificationCenter")
const { PageNotifyModel } = require("../../../../dist/models/pageNotifyModel");

// app/pages/component-page/c-page/index.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
    },
    tapSendNotify(e) {
        let pageNotifyModel;
        if (e.currentTarget.dataset.type == 'auto') {
            pageNotifyModel = new PageNotifyModel({
                title: "自动关闭",
                bgColor: "#ff2525",
                autoClose: true,
                fn: function(){
                    console.log("自动关闭点击")
                }
            })
        } else {
            pageNotifyModel = new PageNotifyModel({
                title: "手动关闭",
                bgColor: "#ff2525",
                autoClose: false,
                fn: function(){
                    console.log("手动关闭点击")
                }
            })
        }
        postNotification(KEY_NOTIFICATION_PAGE_NOTIFY_SHOW, {show: true, pageNotifyModel})
    }
}) 