// app/pages/component-page/c-list/index.js
Page({

    /**
     * 页面的初始数据
     */
    data: {
        listData: [1,2,3,4,5,6,7]
    },

    tapDelete(e) {
        this.data.listData.splice(e.currentTarget.dataset.index, 1);
        this.setData({
            listData: this.data.listData
        })
    }
})