const { getOption } = require("../../options")

let develop = function(){
    return {
        appName: getOption()? getOption().appName.dev : "", // app名称
        appSlogan: getOption()? getOption().appSlogan.dev : "", // app slogan
        appLogoPath: getOption()? getOption().appLogoPath.dev : "", // appLogo 
        Platfom_Service_Phone: getOption()? getOption().platfomServicePhone.dev : "", // 平台客服电话 
        Base_Url: getOption()? getOption().request.url.dev : "", // 基础路径
        Base_Socket_Url: getOption()? getOption().request.socketUrl.dev : "", // socket 基础路径
        Print_Level: getOption()? getOption().printLevel.dev : "", // 打印级别
        Show_Debug_Page: getOption()? getOption().showDebugPage.dev : "", // 是否显示调试信息页面
    }
}

let getDevelop = develop

/**
 * 开发环境配置
 */
module.exports = {getDevelop}