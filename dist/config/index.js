import {getEnv} from "./env/env";
import {getProduct} from "./env/product";
import {getDevelop} from "./env/develop";
import {getTrial} from "./env/trial";
const { getOption } = require("../options");

const env = getEnv("envVersion"); // 获取环境
let config = function(){
    let envStr;
    let envConfig;
    if (env == 'develop') {
        envConfig = getDevelop(); // 开发环境 -- 微信审核陈年BUG，审核看到的是develop，可能导致审核不通过
        envStr = "Dev";
    } else if (env == 'trial') {
        envConfig = getTrial(); // 体验环境
        envStr = "Test";
    } else { 
        envConfig = getProduct(); // 生产环境
        envStr = "Prod";
    }
    return {
        ...envConfig, // 配置参数
        env: envStr, // 环境名称 
        version: getOption()? getOption().version.version: "", // 版本号 小功能调整、BUG修复，提交上线后修改小版本；新功能添加，提交上线后修改中版本；大变动，提交上线后修改大版本
        buildCode: getOption()? getOption().version.buildCode: 0, // 构建号 同一版本，上线前，每次调整提交后修改构建号
        needRefreshStorageVersion: getOption()? getOption().version.needRefreshVersion: null, // 需要刷新缓存的最低版本号，null 为当前版本
        needRefreshStorageBuildCode: getOption()? getOption().version.needRefreshBuildCode: null, // 需要刷新缓存的最低构建号， null 为任意构建号
    }
}
let getConfig = config;
module.exports = {
    getConfig
}