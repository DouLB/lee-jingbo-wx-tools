const { KEY_STORAGE_USERINFO } = require("../keys/storage-key");
const { toolPrintError } = require("../utils/printUtil");
const { saveStorage, readStorage } = require("../utils/storageUtil");
const { postNotification, addNormalNotificationObserver, removeNotificationObserver } = require("../utils/notificationCenter");
const { KEY_NOTIFICATION_LOGIN_VIEW_SHOW, KEY_NOTIFICATION_USERINFO_CHANGE } = require("../keys/notification-key");
const { getOption } = require("../options")
const { apiPOST, apiGET, apiPUT, apiDELETE } = require("../request/apiRequest");

let storageUser = null; // 缓存的用户数据
let needRefreshStorageUserInfo = false; // 是否需要刷新userInfo数据

/**
 * 请求登录
 * @param {object} param wxCode 微信code | callback 请求回调
 */
function sysLogin(param={code, callback, complete, dataHandlerFn}) {
    if(!param || !param.code) {
        throw new Error("登陆 wx code 不能为空");
    }
    // 请求登录
    apiPOST({
        url: `${getOption().login.sysLoginUrl}/${code}`,
        urlModule: `${getOption().login.sysLoginUrlModule}`,
        callback: (success, data)=>{
            if (success) {
                if (typeof dataHandlerFn == 'function') {
                    data = dataHandlerFn(data);
                }
                saveUserInfo(data);
                if (typeof callback == 'function') callback(true, data);
            } else {
                if (typeof callback == 'function') callback(false, data);
            }
        },
        complete
    })
}

/**
 * 请求注册
 * @param {object} param param 注册参数 | callback 请求糊掉 
 */
function sysRegister(param={paramData, callback, complete, dataHandlerFn}) {
    if(!param || !param.paramData) {
        throw new Error("注册参数 不能为空");
    }
    // 请求注册
    apiPOST({
        url: `${getOption().login.sysRegisterUrl}`,
        urlModule: `${getOption().login.sysRegisterUrlModule}`,
        data: {...param},
        callback: (success, data)=>{
            if (success) {
                if (typeof dataHandlerFn == 'function') {
                    data = dataHandlerFn(data);
                }
                saveUserInfo(data);
                if (typeof callback == 'function') callback(true, data);
            } else {
                if (typeof callback == 'function') callback(false, data);
            }
        },
        complete
    })
}

/** 用户信息刷新 */
function sysUserRefresh(param={dataHandlerFn, callback}) {
    apiPOST({
        url: `${getOption().login.userInfoRefreshUrl}`,
        urlModule: `${getOption().login.userInfoRefreshUrlModule}`,
        callback: (success, data)=>{
            if (success) {
                if (typeof dataHandlerFn == 'function') {
                    data = dataHandlerFn(data);
                }
                saveUserInfo(data);
                if (typeof callback == 'function') callback(true, data);
            } else {
                if (typeof callback == 'function') callback(false, data);
            }
        }
    })
}

/** 保存用户信息 */
function saveUserInfo(userInfo) {
    let cUser = readUserInfoSync();
    if (!cUser) cUser = {};
    cUser = {
        ...cUser,
        ...userInfo
    }
    saveStorage(KEY_STORAGE_USERINFO, cUser);
    needRefreshStorageUserInfo = true;
    postNotification(KEY_NOTIFICATION_USERINFO_CHANGE, {userInfo: cUser})
}

/** 同步获取用户信息 可能为null 只获取一次 当前存储的用户信息 */
function readUserInfoSync() {
    if (!storageUser || needRefreshStorageUserInfo) {
        try {
            storageUser = readStorage(KEY_STORAGE_USERINFO);
            needRefreshStorageUserInfo = false;
        } catch (error) {
            toolPrintError("readUserInfo", error.errMsg);
        }
    }
    return storageUser;
}

/** 异步读取用户信息 会持续监听用户信息变化 */
function readUserInfoAsync(watchFn, observer) {
    if (typeof watchFn == 'function' && observer) {
        addNormalNotificationObserver(KEY_NOTIFICATION_USERINFO_CHANGE, watchFn, observer);
    }
    if (!storageUser || needRefreshStorageUserInfo) {
        storageUser = readStorage(KEY_STORAGE_USERINFO);
        needRefreshStorageUserInfo = false;
    }
    if (observer) {
        watchFn({userInfo: storageUser});
    }
}
/** 移除用户信息监听 */
function removeUserInfoChangeWatch(observer) {
	removeNotificationObserver(KEY_NOTIFICATION_USERINFO_CHANGE, observer);
}

/**
 * 展示登录页
 * @param {boolean} show 是否显示登录页面
 * @param {*} info 附带信息
 */
function showLoginView(show, info) {
    postNotification(KEY_NOTIFICATION_LOGIN_VIEW_SHOW, {show,...info})
}

/**
 * 展示关注公众号弹窗
 * @param {boolean} show 
 */
function showOfficialAccountPop(show) {
    postNotification(KEY_NOTIFICATION_OFFICIAL_ACCOUNT_POP_SHOW, {show})
}

module.exports = {
    showLoginView,
    
    sysLogin,
    sysRegister,
    sysUserRefresh,

    saveUserInfo,
    readUserInfoSync,
    readUserInfoAsync,

    removeUserInfoChangeWatch,

    showOfficialAccountPop,
}