
let globalOptions;

function resetOptions(options) {
    globalOptions = options
}

function getOption(options) {
    return globalOptions;
}

module.exports = {
    resetOptions,
    getOption,
}