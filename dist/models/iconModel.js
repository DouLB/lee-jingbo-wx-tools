let IconModel = function() {
    this.prefix = "van-icon"; // icon prefix
    this.name = ""; // icon name
    this.color = "inherit"; // icon 颜色
    this.size = "inherit"; // icon size 默认 px 允许 28rpx 
    this.info = ""; // 右上角文本
    this.dot = false; // 是否显示右上角红点
}

module.exports = {
    IconModel
}