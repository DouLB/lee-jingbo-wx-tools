// dist/components/c-upload-item/index.js
var commonBehavior = require("../../behaviors/commonBehavior");
const { apiUPLOAD } = require("../../request/apiRequest");
const { getOption } = require("../../options");
Component({
    behaviors: [commonBehavior],

    relations: {
        "../c-upload": {
            type: "parent",
            linked: function(target) {
                this.startLinkedTimeout();
            },
            linkChanged: function(target) {

            },
            unlinked: function(target) {
                
            }
        }
    },
    /**
     * 组件的属性列表
     */
    properties: {
        autoUpload: {
            type: Boolean,
            value: true,
        }, // 是否自动上传
        url: {
            type: String,
            value: null
        },
        urlModule: {
            type: String,
            value: null
        },
    },

    /**
     * 组件的初始数据
     */
    data: {
        linkedTimeoutId: null,
        childNodes: null,
    },

    /**
     * 组件的方法列表
     */
    methods: {
        /** 开始 linked 定时器 */
        startLinkedTimeout() {
            this.clearLinkedTimeout();
            this.data.linkedTimeoutId = setTimeout(()=>{
                this.childNodes = this.getRelationNodes('path/to/custom-li');
                if (this.data.autoUpload) {
                    this.startUpload();
                }
            },100)
        },
        /** 清空 linked 定时器 */
        clearLinkedTimeout() {
            if (this.data.linkedTimeoutId) {
                clearTimeout(this.data.linkedTimeoutId);
                this.data.linkedTimeoutId = null;
            }
        },
        /** 开始上传 */
        startUpload() {
            apiUPLOAD({

            })
        }
    }
})
