// components/c-navi/index.js
var commonBehavior = require("../../behaviors/commonBehavior")
Component({
    behaviors: [commonBehavior],
    /**
     * 组件的属性列表
     */
    properties: {
        useSlot: {
            type: Boolean,
            value: false
        },
        naviTitle: {
            type: String,
            value: "",
        }
    },

    externalClasses: ["i-class"],

    /**
     * 组件的初始数据
     */
    data: {
        backButtonTitle: "",
        showBackButton: true,
        naviHeight: 0,
        statusBarHeight: 0,
    },

    observers: {
        "statusBarHeight, naviHeight, useSlot": function(statusBarHeight, naviHeight, useSlot) {
            this.getTriggerEvent("changeHeight", {height: this.data.useSlot? this.data.naviHeight : this.data.naviHeight + this.data.statusBarHeight, statusBarHeight: this.data.statusBarHeight});
        }
    },

    ready() {
        this.setData({
            statusBarHeight: wx.getSystemInfoSync().statusBarHeight
        })
        this.resetNaviHeight();
        this.resetNaviBackButtonShow();
    },

    /**
     * 组件的方法列表
     */
    methods: {
        resetNaviBackButtonShow() {
            let pages = getCurrentPages();
            this.setData({
                showBackButton: pages.length > 1
            })
        },
        resetNaviHeight() {
            const query = wx.createSelectorQuery().in(this);
            query.select("#navi_content").boundingClientRect(res=>{
                if (res && res.height) {
                    this.setData({
                        naviHeight: res.height
                    })
                }
            })
            query.exec();
        },
        tapBack() {
            wx.navigateBack({
              delta: 0,
            })
        }
    }
})