import { dateFormat, getDate, getDateModel, getDayDis, stringToDate } from "../../utils/timeUtil";
const { toolPrintDebug } = require("../../utils/printUtil");
// components/c-week/index.js
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        limitDate: {
            type: Boolean,
            value: false,
        }, // 是否限制日期
        customLimitDateFn: {
            type: Function,
            value: null
        }, // 自定义限制日期方法
        oneWeek: {
            type: Boolean,
            value: false
        }, // 是否只展示一周，不允许滑动
        showDatePicker: {
            type: Boolean,
            value: true
        }, // 是否展示日期选择器
        showWeekDate: {
            type: Boolean,
            value: true
        }, // 是否显示日期, 为false 时 datePicker 也不显示
        badgeData: {
            type: Array,
            value: []
        }, // 徽标数据 [{date: yyyy-mm-dd, count: number, index: number}] oneWeek模式 优先比对 date, date为空比对index
        showBadge: {
            type: Boolean,
            value: true
        }, // 是否展示 badge, badge 数量为0 也不展示
        initCurrentDate: {
            type: Object,
            value: null
        }, // 初识选中数据
        passFutrueColorSeparation: {
            type: Boolean,
            value: true
        }, // 是否使用颜色区分过去、未来
    },

    

    /**
     * 组件的初始数据
     */
    data: {
        weekList: [], // 星期 周列表
        currentDate: null, // 当前选中日期
        currentWeekIndex: 0, // 当前选中日期所在week 在 list 中 下标
        swiperIndex: 0, // swiper index
        limitDateFunction: null, // 默认限制方法
    },

    observers: {
        "initCurrentDate": function(initCurrentDate) {
            if (!initCurrentDate || !initCurrentDate.fullDay) {
                this.attachedInitData();
            } else {
                this.setData({
                    initCurrentDate: stringToDate(initCurrentDate.fullDay)
                })
            }
        },
        "customLimitDateFn": function(customLimitDateFn) {
            if (customLimitDateFn) {
                this.data.limitDateFunction = customLimitDateFn;
                this.initWeekList();
            }
        },
        "passFutrueColorSeparation": function(passFutrueColorSeparation) {
            this.initWeekList();
        },
    },

    created() {
        this.data.limitDateFunction = function(weekList) {
            let nowDate = getDateModel(new Date());
            weekList.forEach(weekItem=>{
                weekItem.forEach(dayItem=>{
                    let dayDis = getDayDis(nowDate.fullDay, dayItem.fullDay);
                    dayItem.isLimit = dayDis > 6;
                })
            })
            return weekList;
        }
    },

    attached() {
        this.attachedInitData();
    },

    /**
     * 组件的方法列表
     */
    methods: {
        attachedInitData(){
            this.setData({
                currentDate: this.dateModel(this.data.initCurrentDate?this.data.initCurrentDate: new Date())
            })
            this.initWeekList(this.data.initCurrentDate? this.data.initCurrentDate: new Date());
            this.triggerEvent("selected", {date: this.data.currentDate});
        },
        /** 处理过去未来时间区分 */
        processPassFutrue(weekList) {
            let currentDate = stringToDate(this.dateModel(new Date()).fullDay);
            return weekList.map(weekItem=>{
                return weekItem.map(dayItem=>{
                    let targetDate = stringToDate(dayItem.fullDay);
                    let compareResult = targetDate - currentDate;
                    if (compareResult != 0) {
                        compareResult = compareResult < 0 ? -1 : 1;
                    }
                    return {
                        ...dayItem,
                        isPassDay: compareResult == -1,
                        isFutrueDay: compareResult == 1
                    }
                })
            })
        },
        /** weekList 数据处理 */
        processWeekList(weekList) {
            if (this.data.passFutrueColorSeparation) {
                weekList = this.processPassFutrue(weekList);
            }
            let processData = this.data.limitDate ? this.data.limitDateFunction(weekList) : weekList;
            return processData;
        },
        /** 初始化星期时间列表 */
        initWeekList(date) {
            let current_week = this.getWeekFromDay(getDate({targetDate: date, weekDuration: 0})); // 当前周 列表
            if (this.data.oneWeek) {
                this.setData({
                    weekList: this.processWeekList([current_week])
                })
            } else {
                let before_week_1 = this.getWeekFromDay(getDate({targetDate: date, weekDuration: -1})); // 前一周 列表
                let after_week_1 = this.getWeekFromDay(getDate({targetDate: date, weekDuration: 1})); // 后一周 列表
                this.setData({
                    weekList: this.processWeekList([ before_week_1, current_week, after_week_1 ]),
                    currentWeekIndex: 1,
                    swiperIndex: 1,
                })
            }
        },
        /** 获取 日期 所在 星期 列表 */
        getWeekFromDay(date) {
            let week = [];
            let weekDay = date.getDay();
            for (let i = 0; i < weekDay; i++) {
                let beforeDate = getDate({
                    targetDate: date,
                    dayDuration: i - weekDay
                })
                week.push(this.dateModel(beforeDate))
            }
            week.push(this.dateModel(date))
            for (let i = weekDay+1; i < 7; i++) {
                let afterDate = getDate({
                    targetDate: date,
                    dayDuration: i - weekDay
                })
                week.push(this.dateModel(afterDate))
            }
            return week;
        },
        /** week 换页  */
        changeSwiper(e) {
            if (e.detail.source == 'touch') {
                let weekList = this.data.weekList;
                // 因为强制切换swiperIndex 的原因， 导致在 滑动到最后一页或第一页再往下滑动的时候， 页面不流畅，再次返回后滑动会再次流畅
                if (e.detail.current == 0) { // 到第一页 需要加载更早之前的时间
                    let newWeek = this.getWeekFromDay(getDate({targetDate: this.data.currentDate.date, weekDuration: 0-(this.data.currentWeekIndex + 1)})); // 根据当前选中的日期所在 星期 在列表中的index 计算 新的week 数据
                    weekList.unshift(newWeek); // 插入头部
                    this.setData({
                        currentWeekIndex: this.data.currentWeekIndex + 1, // 因为头部插入一条新数据，所以currentWeekIndex + 1
                        swiperIndex: 1, // 因为头部插入新数据， 所以当前页 归 1
                    })
                } else if (e.detail.current == this.data.weekList.length - 1 ) { // 到最后一页 需要加载更晚的时间
                    let newWeek = this.getWeekFromDay(getDate({targetDate: this.data.currentDate.date, weekDuration: this.data.weekList.length - this.data.currentWeekIndex - 1})); // 根据当前选中的日期所在 星期 在列表中的index 计算 新的week 数据
                    weekList.push(newWeek); // 尾部追加
                    this.setData({
                        swiperIndex: weekList.length-2 // 因为尾部追加新数据， 所以 swiperIndex 后退一格
                    })
                }
                this.setData({
                    weekList: this.processWeekList(weekList)
                })
            }
        },
        /** 修改日期 */
        changeDate(e) {
            let date = stringToDate(e.detail.value);
            this.setData({
                currentDate: this.dateModel(date)
            })
            this.initWeekList(date);
            this.triggerEvent("selected",{date: this.data.currentDate})
        },
        /**
         * 日期对象
         * @param {date} date Date
         */
        dateModel(date) {
            return getDateModel(date);
        },
        /** 选择日期 */
        tapDay(e) {
            if (e.currentTarget.dataset.dayitem.isLimit) return; // 如果被限制，不允许点击
            this.setData({
                // dataset 丢失了 dayItem 中的 date, 应该是因为 Date 不能进行 复制的原因, 导致无法传递， 所以通过fullDay字段进行重新赋值
                currentDate: this.dateModel(stringToDate(e.currentTarget.dataset.dayitem.fullDay)), 
                currentWeekIndex: e.currentTarget.dataset.weekindex,
            })
            this.triggerEvent("selected",{date: this.data.currentDate})
        },
        reTrigger() {
            this.triggerEvent("selected", {date: this.data.currentDate})
        }
    }
})
