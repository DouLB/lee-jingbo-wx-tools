// components/c-list/index.js
const DefaultPageIndex = 1;
const {
    showLoginView, readUserInfoAsync
} = require("../../services/userServices");
const {
    toolPrintDebug
} = require("../../utils/printUtil");
const { apiRequest } = require("../../request/apiRequest");
var commonBehavior = require("../../behaviors/commonBehavior")
Component({
    behaviors: [commonBehavior],
    /**
     * 组件的属性列表
     */
    properties: {
        listHeight: {
            type: Number,
            value: 0
        }, // 列表高度
        header: {
            type: Object,
            value: {}
        }, // 请求头
        method: {
            type: String,
            value: "GET"
        }, // 请求method
        queryData: {
            type: Object,
            value: {}
        }, // 请求参数
        url: {
            type: String,
            value: ""
        }, // 请求路径
        urlModule: {
            type: String,
            value: null
        }, // 请求模块名称
        refreshOnLoad: {
            type: Boolean,
            value: true
        }, // 是否在load时进行刷新
        emptyMessage: {
            type: String,
            value: "数据为空"
        }, // empty页面提示语
        emptyLogin: {
            type: Boolean,
            value: false
        }, // empty页面是否展示登录按钮
        ableLoadmore: {
            type: Boolean,
            value: true,
        }, // 是否允许加载更多
        ableRefresh: {
            type: Boolean,
            value: true,
        }, // 是否允许刷新
        listData: {
            type: Array,
            value: null
        }, // 初始化数据 会对内部的dataList 进行覆盖
    },

    options: {
        multipleSlots: true // 在组件定义时的选项中启用多slot支持
    },

    observers: {
        listData: function (listData) {
            this.setData({
                dataList: listData
            })
        },
        dataList: function (dataList) {
            setTimeout(() => {
                this.resetLoadMoreHeight();
            }, 100);
        },
    },

    /**
     * 组件的初始数据
     */
    data: {
        placeholderHeight: 0, // 占位空间高度，如果scroll内容没有充满一屏，无法拉起loadMore, 使用空白页面撑开
        isRefreshing: false, // 设置下拉状态
        isLoadMore: false, // 设置加载状态
        pageIndex: DefaultPageIndex, // 页码
        pageSize: 30, // 页长
        dataList: [], // 数据
        total: 0, // 数据总长度
        alreadyRefreshOnLoad: false, // 是否已经在load时进行刷新
        userInfo: null,
    },

    lifetimes: {
        attached() {
            this.attachedFn();
        }
    },

    attached(){
        this.attachedFn();
    },

    /**
     * 组件的方法列表
     */
    methods: {
        attachedFn() {
            let $this = this;
            readUserInfoAsync((info)=>{
                $this.setData({
                    userInfo: info.userInfo
                })
            }, this);
            if (this.data.listHeight == 0) {
                this.setData({
                    listHeight: wx.getSystemInfoSync().windowHeight
                })
            }
            if (this.data.refreshOnLoad && !this.data.alreadyRefreshOnLoad) {
                this.onPullRefreshing();
            }
        },
        /** 计算是否需要显示list bottom */
        resetLoadMoreHeight() {
            const query = wx.createSelectorQuery().in(this);
            query.select(".c-list-view-content").boundingClientRect(res => {
                if (res) {
                    this.setData({
                        placeholderHeight: this.data.listHeight - res.height
                    })
                } else {
                    this.setData({
                        placeholderHeight: this.data.listHeight
                    })
                }
            });
            query.exec();
        },
        /** 下拉刷新 */
        onPullRefreshing() {
            this.getTriggerEvent("tapPull");
            if (this.data.ableRefresh && this.data.url) {
                if (this.data.isLoadMore || this.data.isRefreshing) return;
                this.setData({
                    isRefreshing: true
                })
                this.data.pageIndex = DefaultPageIndex;
                this.requestListData();
            } else {
                this.setData({
                    isRefreshing: false
                })
            }
        },
        /** 上拉加载 */
        onReachBottom() {
            if (this.data.ableLoadmore && this.data.url) {
                if (this.data.dataList.length >= this.data.total) return;
                if (this.data.isLoadMore || this.data.isRefreshing) return;
                this.setData({
                    isLoadMore: true
                })
                this.requestListData();
            } else {
                this.setData({
                    isLoadMore: false
                })
            }
        },
        /** 请求列表数据 */
        requestListData() {
            let param = {pageIndex: this.data.pageIndex, pageSize: this.data.pageSize};
            if (this.data.queryData) param = {...param, ...this.data.queryData};
            toolPrintDebug(param);
            apiRequest({
                url: this.data.url,
                data: {...param},
                method: this.data.method,
                header: this.header?this.header:{},
                callback: (success, data)=>{
                    this.setData({
                        isRefreshing: false,
                        isLoadMore: false
                    })
                    if (success) {
                        let rows = data.records;
                        let total = data.total;
                        if (!rows) {
                            rows = data;
                            total = data.length
                        }
                        if (this.data.pageIndex == 1) {
                            this.setData({
                                dataList: rows,
                                total
                            })
                        } else {
                            this.setData({
                                dataList: this.data.dataList.concat(rows),
                                total
                            })
                        }
                        this.getTriggerEvent("dataChange", {
                            isRefreshing: this.data.isRefreshing, 
                            isLoadMore: this.data.isLoadMore, 
                            data: this.data.dataList, 
                            total: this.data.total, 
                            pageIndex: this.data.pageIndex
                        })
                        this.pageIndex = this.pageIndex + 1;
                    }
                    
                    if (!this.data.alreadyRefreshOnLoad && (!this.data.dataList || this.data.dataList.length <= 0)) {
                        this.data.alreadyRefreshOnLoad = true;
                    }
                }
            })
        },
        /** 点击登录 */
        tapLogin() {
            showLoginView(true);
        },
    },
})