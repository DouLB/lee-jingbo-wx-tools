// components/c-image/index.js
var commonBehavior = require("../../behaviors/commonBehavior")
Component({
    behaviors: [commonBehavior],
  /**
   * 组件的属性列表
   */
  properties: {
    src: {
      type: String,
      value: ""
    },
    mode: {
      type: String,
      value: 'scaleToFill'
    },
    lazyload: {
      type: Boolean,
      value: true
    },
    showMenuByLongpress: {
      type: Boolean,
      value: false
    }
  },
  externalClasses: ["i-class"],
  observers: {
    "src": function(src) {
      if (src !== this.data.src) {
        this.setData({
          loadSuccess: false
        })
      }
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    loadSuccess: false
  },

  /**
   * 组件的方法列表
   */
  methods: {
    error(e) {
      this.setData({
        loadSuccess: false
      })
    },
    load(e) {
      this.setData({
        loadSuccess: true
      })
    }
  }
})
