// components/c-login/index.js
const {
    toolPrintDebug, toolPrintError
} = require("../../utils/printUtil");
const {
    showLoginView, sysRegister, sysLogin, saveUserInfo,
} = require("../../services/userServices");
const {
    addNormalNotificationObserver,
    removeNotificationObserver,
    postNotification
} = require("../../utils/notificationCenter");
const { KEY_NOTIFICATION_LOGIN_VIEW_SHOW, KEY_NOTIFICATION_LOGIN_SUCCESS } = require("../../keys/notification-key");
var commonBehavior = require("../../behaviors/commonBehavior");
const { getOption } = require("../../options");
import {getConfig} from "../../config/index";
Component({
    behaviors: [commonBehavior],
    /**
     * 组件的属性列表
     */
    properties: {

    },

    /**
     * 组件的初始数据
     */
    data: {
        show: false,
        boxType: "login", // 登录框状态 login getPhone register chooseRole 
        loginLoading: false, // 登录中
        gettingPhone: false, // 获取电话中
        gettingInfo: false, // 获取用户信息
        wxCode: null, // 微信code
        phoneCode: null, // phone Code
        phoneEncryptedData: null,
        phoneIv: null,
        openId: null, 
        unionId: null,
        sessionKey: null,
        userInfo: null, // 用户信息

        appName: "", //
        appSlogan: "", // 
        appLogo: "",
    },

    observers: {
        "show": function (show) {
            if (show) {
                wx.hideTabBar({
                    boxType: "login",
                    animation: false,
                }).catch((error)=>{
                    toolPrintError('hide tab bar fail', error)
                })
            } else {
                this.resetPageData();
                wx.showTabBar({
                    animation: false,
                }).catch((error)=>{
                    toolPrintError('show tab bar fail',error)
                })
            }
        }
    },
    created() {
        addNormalNotificationObserver(KEY_NOTIFICATION_LOGIN_VIEW_SHOW, (info) => {
            this.setData({
                show: info.show
            })
        }, this);
    },

    ready() {
        let config = getConfig();
        this.setData({
            appName: getConfig().appName,
            appSlogan: getConfig().appSlogan,
            appLogo: getConfig().appLogoPath
        })
    },

    detached() {
        removeNotificationObserver(KEY_NOTIFICATION_LOGIN_VIEW_SHOW, this);
    },

    /**
     * 组件的方法列表
     */
    methods: {
        /** 重置初始化数据，页面关闭时执行 */
        resetPageData() {
            this.setData({
                boxType: "login", // 登录框状态 login getPhone register chooseRole 
                loginLoading: false, // 登录中
                gettingPhone: false, // 获取电话中
                gettingInfo: false, // 获取用户信息
                wxCode: null, // 微信code
                phoneCode: null, // phone Code
                phoneEncryptedData: null,
                phoneIv: null,
                openId: null, 
                unionId: null,
                sessionKey: null,
                userInfo: null, // 用户信息
            })
        },
        /** 登录成功 */
        loginSuccess(userInfo) {
            let afterLoginSuccess = getOption().login.afterLoginSuccess
            if (afterLoginSuccess && typeof afterLoginSuccess == 'function') {
                afterLoginSuccess(userInfo, (success, res)=>{
                    if (success) {
                        this.saveUserInfo(res)
                    }
                })
            } else {
                this.saveUserInfo(userInfo)
            }
        },
        /** 点击遮罩 */
        tapMask() {
            showLoginView(false);
        },
        /** 取消登录 */
        tapCancelLogin() {
            showLoginView(false);
        },
        /** 点击登录 */
        tapLogin() {
            this.setData({
                loginLoading: true
            })
            this.wxLogin().then(wxCode => {
                this.setData({
                    wxCode
                })
                // 使用 wxCode 发起登录 request-login
                // 登录成功 => 返回用户信息并存储
                // this.saveUserInfo();
                // 未注册 =>
                sysLogin({
                    code: wxCode,
                    callback: (success, data)=>{
                        this.setData({
                            loginLoading: false
                        })
                        if (success) {
                            if (data.success) { // 登录成功
                                this.loginSuccess(data);
                            } else { // 未注册
                                this.setData({
                                    unionId: data.unionId,
                                    openId: data.openId,
                                    // sessionKey: data.sessionKey,
                                    boxType: 'getInfo'
                                })
                            }
                        }
                    }
                })
            }).catch((error) => {
                toolPrintDebug(error.errMsg);
                this.setData({
                    loginLoading: false
                })
            })
        },
        /** 取消获取手机号 */
        tapCancelGetPhone() {
            this.setData({
                gettingPhone: false,
                boxType: 'login'
            })
        },
        /** 点击获取手机号 */
        tapGetPhone(e) {
            this.setData({
                gettingPhone: true
            })
            if (e.detail.code) {
                this.setData({
                    phoneCode: e.detail.code
                })
            } else if (e.detail.encryptedData) {
                this.setData({
                    phoneEncryptedData: e.detail.encryptedData,
                    phoneIv: e.detail.iv
                })
            }
            this.requestRegister();
        },
        /** 取消用户信息授权 */
        tapCancelGetUserPorfile() {
            this.setData({
                gettingInfo: false,
                boxType: 'login'
            })
        },
        /** 获取用户信息 */
        tapGetUserPorfile() {
            this.setData({
                gettingInfo: true
            })
            this.wxGetUserProfile().then((res) => {
                toolPrintDebug("用户信息", res);
                this.setData({
                    nickName: res.userInfo.nickName,
                    avatarUrl: res.userInfo.avatarUrl,
                    boxType: "getPhone",
                    gettingInfo: false
                })
            }).catch((error) => {
                toolPrintError(error.errMsg);
                this.setData({
                    gettingInfo: false
                })
            })
        },
        /** 请求注册 */
        requestRegister() {
            let registerParam = {
                openId: this.data.openId,
                unionId: this.data.unionId,
                // sessionKey: this.data.sessionKey,
                nickName: this.data.nickName,
                avatar: this.data.avatarUrl,
                code: this.data.phoneCode,
                encryptedData: this.data.phoneEncryptedData,
                iv: this.data.phoneIv
            }
            // 发起注册请求 request-register
            // 用户昵称 头像 电话 openid unionid 返回用户信息并存储
            sysRegister({
                param: registerParam,
                callback: (success, data) => {
                    this.setData({
                        gettingPhone: false
                    })
                    if (success) {
                        this.loginSuccess(data);
                    }
                }
            })
        },
        /** 保存用户信息 */
        saveUserInfo(userInfo) {
            saveUserInfo(userInfo);
            showLoginView(false);
            postNotification(KEY_NOTIFICATION_LOGIN_SUCCESS, {userInfo});
        },
        /** 微信登录 */
        wxLogin() {
            return new Promise((resolve, reject) => {
                wx.login({
                    success: (res) => {
                        resolve(res.code);
                    },
                    fail: (error) => {
                        reject(error);
                    }
                })
            })
        },
        /** 微信获取用户基本信息 */
        wxGetUserProfile() {
            return new Promise((resolve, reject) => {
                wx.getUserProfile({
                    desc: '完善会员资料',
                    success: (res) => {
                        resolve(res);
                    },
                    fail: (error) => {
                        reject(error)
                    }
                })
            })
        },
    }
})