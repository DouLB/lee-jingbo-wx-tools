// components/c-page/index.js
const { addNormalNotificationObserver, removeNotificationObserver, postNotification } = require("../../utils/notificationCenter")
const { KEY_NOTIFICATION_PAGE_NOTIFY_SHOW } = require("../../keys/notification-key");
const { showPageNotify } = require("../../services/commonNotifyServices");
const DEFAULT_CLOSE_TIMEOUT_DELAY = 5000;
const commonBehavior = require("../../behaviors/commonBehavior");
Component({
    behaviors: [commonBehavior],
    /**
     * 组件的属性列表
     */
    properties: {
        useCustomNavi: {
            type: Boolean,
            value: false
        }, // 是否使用自定义navi，页面JSON 或者 app.JSON 需要配置navigationBarStyle
        naviTitle: {
            type: String,
            value: "",
        }, // c-navi 的 title
        useNaviSlot: {
            type: Boolean,
            value: false
        }, // 是否使用c-navi的slot插槽
    },

    /**
     * 组件的初始数据
     */
    data: {
        showPageNotify: false, // 是否显示page通知
        pageNotify: null, // 通知对象
        bgColorStyle: "", // 通知背景样式
        closeTimeoutId: null, // 通知关闭定时器
    },

    observers: {
        "showPageNotify": function(showPageNotify){
            if (!showPageNotify) {
                this.clearCloseTimeout();
            }
        }
    },

    created() { 
        addNormalNotificationObserver(KEY_NOTIFICATION_PAGE_NOTIFY_SHOW, (info)=>{
            this.setData({
                showPageNotify: info.show,
                pageNotify: info.pageNotifyModel,
                bgColorStyle: info.pageNotifyModel && info.pageNotifyModel.bgColor? `background: ${info.pageNotifyModel.bgColor};`: "",
            })
            if (this.data.pageNotify && this.data.pageNotify.autoClose) {
                this.clearCloseTimeout();
                this.closeTimeoutId = setTimeout(() => {
                    showPageNotify(false);
                }, DEFAULT_CLOSE_TIMEOUT_DELAY);
            } else {
                this.clearCloseTimeout();
            }
        }, this);
    },

    detached() {
        removeNotificationObserver(KEY_NOTIFICATION_PAGE_NOTIFY_SHOW, this);
    },

    /**
     * 组件的方法列表
     */
    methods: {
        // 自定义 navi 高度变化
        naviHeightChange(e) {
            this.getTriggerEvent("changeHeight", e);
        },
        // 关闭定时器
        clearCloseTimeout() {
            if (this.data.closeTimeoutId) {
                clearTimeout(this.data.closeTimeoutId);
                this.data.closeTimeoutId = null;
            }
        },
        // 点击 page 通知栏
        tapPageNotify() {
            if (typeof this.data.pageNotify.fn == 'function') this.data.pageNotify.fn();
            postNotification(KEY_NOTIFICATION_PAGE_NOTIFY_SHOW, {show: false})
        },
    },

    externalClasses: ["i-class"]
})
