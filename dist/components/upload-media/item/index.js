// components/upload-media/item/index.js
const { downloadVideo, checkAuthSetting } = require("../../../utils/util");
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    mediaObj: {
      type: Object,
      value: {
        url: '', // media 地址
        thumbUrl: '', // video 封面图url
        status: '', // 状态 unUpload 未上传 success 上传成功 error 上传失败 uploading 上传中
        progress: 0, // 上传进度
        type: 'image', // media 类型 image 图片 video 视频
        name: '', // 文件名 包含扩展
      }
    },
    ableDelete: {
      type: Boolean,
      value: false,
    },
    mediaIndex: {
      type: Number,
      value: 0
    },
    progressSize: {
      type: Number,
      value: 10
    },
    progressColor: {
      type: String,
      value: '#00aaff'
    },
    progressBGColor: {
      type: String,
      value: '#eee'
    },
    muted: {
        type: Boolean,
        value: true
    },
    ableSaveVideo: {
      type: Boolean,
      value: false
    },
    ableSelected: {
      type: Boolean,
      value: false
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    videoContext: null
  },

  /**
   * 组件的方法列表
   */
  methods: {
    tapSelected() {
      this.triggerEvent("selected", {obj: {...this.data.mediaObj, selected: !this.data.mediaObj.selected}, index: this.data.mediaIndex})
    },
    tapImage() {
      this.triggerEvent('tapImage', {obj: this.data.mediaObj, index: this.data.mediaIndex})
    },
    tapVideo() {
      if (this.data.videoContext === null) {
        this.data.videoContext = wx.createVideoContext('upload-video-id', this);
      }
      this.data.videoContext.requestFullScreen();
      this.data.videoContext.play();
    },
    tapDelete() {
      this.triggerEvent('delete', {obj: this.data.mediaObj, index: this.data.mediaIndex})
    },
    tapDownload() {
      wx.showModal({
        title: "下载",
        content: "是否要保存该视频到本地？",
        success: (res)=>{
          if (res.confirm) {
            wx.showLoading({
              title: '保存中',
            })
            checkAuthSetting("writePhotosAlbum", (success, msg)=>{
              if (success) {
                downloadVideo(this.data.mediaObj.serverObj.imageUrl, (downloadSuccess, downloadMsg)=>{
                  if (downloadSuccess) {
                    wx.showToast({
                      title: '保存成功'
                    })
                  } else {
                    wx.hideLoading({
                      success: (res) => {},
                    })
                    wx.showModal({
                      title: "保存失败",
                      content: downloadMsg,
                      showCancel: false,
                    })
                  }
                })
              } else {
                wx.hideLoading({
                  success: (res) => {},
                })
                wx.showModal({
                  title: "未授权保存",
                  content: msg,
                  showCancel: false,
                })
              }
            })
          }
        }
      })
    },
    tapMask() {
      
    },
    tapRetry() {
      this.triggerEvent('retry', {obj: this.data.mediaObj, index: this.data.mediaIndex})
    },
    fullScreenChange(e) {
      if (!e.detail.fullScreen) {
        this.data.videoContext.pause();
      }
    }
  }
})
