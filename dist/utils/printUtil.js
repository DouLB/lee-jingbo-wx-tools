const {
    ENUM_PRINT_LEVEL
} = require("../enum/optionsEnum");
import {
    getConfig
} from "../config/index";
/** info 打印 */
function toolPrintInfo(...args) {
    if (getConfig().Print_Level <= ENUM_PRINT_LEVEL.INFO) {
        console.log("INFO =>", ...args);
    }
}
/** debug 打印 */
function toolPrintDebug(...args) {
    if (getConfig().Print_Level <= ENUM_PRINT_LEVEL.DEBUG) {
        console.log("DEBUG =>", ...args);
    }
}
/** error 打印 */
function toolPrintError(...args) {
    if (getConfig().Print_Level <= ENUM_PRINT_LEVEL.ERROR) {
        console.log("ERROR =>", ...args);
    }
}

/** 页面/组件 打印 */
function pagePrint(response, ...args) {
    toolPrintInfo(response.is, ...args);
}
const DEBUG_INFO_LIST = [];
/** 记录 debug 信息 */
function saveDebugRecord(...args) {
    let string = "";
    args.forEach(item => {
        if (typeof item == 'object') item = JSON.stringify(item);
        if (string) string = `${string},`;
        string = `${string}${item}`;
        DEBUG_INFO_LIST.unshift(string);
    })
}
/** 获取 debug 记录 */
function getDebugRecord() {
    return DEBUG_INFO_LIST;
}

module.exports = {
    toolPrintInfo,
    toolPrintDebug,
    toolPrintError,

    pagePrint,

    saveDebugRecord,
    getDebugRecord,
}