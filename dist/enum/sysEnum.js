
const ENUM_TOAST_TYPE = {
    LOADING: "loading",
    SUCCESS: 'success',
    FAIL: 'fail',
    MESSAGE: 'message'
} // Toast 弹窗

const ENUM_MEDIA_TYPE = {
    IMAGE: "1",
    VIDEO: "2",
    UNKNOW: "99",
} // 媒体类型

const ENUM_UPLOAD_MEDIA_STATUS = { // unUpload 未上传 success 上传成功 error 上传失败 uploading 上传中
    UNUPLOAD: "unUpload", // 未上传
    UPLOADING: "uploading", // 上传中
    SUCCESS: "success", // 上传成功
    ERROR: "error", // 上传错误
}

module.exports = {
    ENUM_TOAST_TYPE,
    ENUM_MEDIA_TYPE,
    ENUM_UPLOAD_MEDIA_STATUS
}