const ENUM_ROLE = {
    CUSTOMER: "1", // 客户
    TEMP_STATION: "2", // 未认证站点
    STATION: "3", // 站点
} // 系统角色
const ENUM_ROLE_CUSTOMER = {
    NORMAL: "1", // 普通
    VIP: "2", // vip
} // 系统角色 客户 子角色
const ENUM_ROLE_STATION = {
    MANAGER: "1", // 管理员
    SERVICE: "2", // 客服
    DRIVER: "3", // 司机
} // 系统角色 站点 子角色
const ENUM_ROLE_TEMP_STATION = {
    MANAGER: "1", // 管理员
    SERVICE: "2", // 客服，
    DRIVER: "3", // 司机
} // 系统角色 临时站点 子角色
const ENUM_ROLE_CARROUTE = {
    MANAGER: "1",
    SERVICE: "2",
    DRIVER: "3",
} // 系统角色 运输公司 子角色
const ENUM_ROLE_BUSINESS = {
    MANAGER: "1",
    SERVICE: "2"
} // 系统角色 商家 子角色

module.exports = {
    ENUM_ROLE,
    ENUM_ROLE_CUSTOMER,
    ENUM_ROLE_STATION,
    ENUM_ROLE_TEMP_STATION,
    ENUM_ROLE_CARROUTE,
    ENUM_ROLE_BUSINESS,
}