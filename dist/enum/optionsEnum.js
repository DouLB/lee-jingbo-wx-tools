const ENUM_PRINT_LEVEL = {
    INFO: 1,
    DEBUG: 2,
    ERROR: 3
}

module.exports = {
    ENUM_PRINT_LEVEL,
}